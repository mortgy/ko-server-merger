#pragma once

#include "Account.h"

class Server;
class Accounts
{
	typedef ci_map<Account>::type AccountMap;

public:
	INLINE size_t GetSize() { return m_accountMap.size(); } 

	bool Load(Server * server);
	Account * LookupAccount(const std::string& strAccountID);

protected:
	AccountMap m_accountMap;
};
