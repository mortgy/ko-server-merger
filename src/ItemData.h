#pragma once

#pragma pack(push, 1)
struct RawItemData
{
	uint32 nItemID;
	uint16 sDurability;
	uint16 sCount;
};
#pragma pack(pop)

typedef union
{
	uint64		i;
	uint8		b[8];
} MYINT64;

typedef std::set<uint64> SerialSet;
