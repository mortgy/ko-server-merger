#include "stdafx.h"

Merger::Merger()
	: m_src("src", "knight", "knight"),
	  m_dst("dst", "knight", "knight")
{
}

bool Merger::Run()
{
	if (!m_src.Connect()
		|| !m_dst.Connect()
		|| !m_accounts.Load(&m_src)
		|| !m_accounts.Load(&m_dst))
		return false;

	printf("Total unique accounts loaded: %u\n", 
		m_accounts.GetSize());

	// Stage 1: go through and fix all duplicate serials
	// so there's no collisions across servers.
	if (!FixDuplicateSerials())
		return false;

	printf("Done, shutting down...\n");
	return true;
}

bool Merger::FixDuplicateSerials()
{
	if (!m_src.LoadItems()
		|| !m_dst.LoadItems())
		return false;

	SerialSet serials;

	printf("Loading serials from destination database...\n");
	m_dst.FixDuplicateSerials(serials);

	printf("Loading serials from source database...\n");
	m_src.FixDuplicateSerials(serials);

	serials.clear();
	return true;
}
