#pragma once

class Merger
{
public:
	INLINE Accounts * GetAccounts() { return &m_accounts; }

	Merger();
	bool Run();
	bool FixDuplicateSerials();

protected:
	Accounts m_accounts;
	Server m_src, m_dst;
};
