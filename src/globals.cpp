#include "stdafx.h"
#include <algorithm>
#include <cctype>
#include <functional>

template<int (&F)(int)> int safe_ctype(unsigned char c) { return F(c); } 
static int safe_isspace(int c) { return safe_ctype<std::isspace>(c); }

// trim from end
std::string & rtrim(std::string &s) 
{
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(safe_isspace))).base(), s.end());
	return s;
}

// trim from start
std::string & ltrim(std::string &s) 
{
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(safe_isspace))));
	return s;
}

void strtolower(std::string& str)
{
	std::transform(str.begin(), str.end(), str.begin(), tolower);
}

void strtoupper(std::string& str)
{
	std::transform(str.begin(), str.end(), str.begin(), toupper);
}

int myrand(int min, int max)
{
	if( min == max ) return min;
	if( min > max )
	{
		int temp = min;
		min = max;
		max = temp;
	}

	double gap = max - min + 1;
	double rrr = (double)RAND_MAX / gap;

	double rand_result;

	rand_result = (double)rand() / rrr;

	if( (int)( min + (int)rand_result ) < min ) return min;
	if( (int)( min + (int)rand_result ) > max ) return max;

	return (int)( min + (int)rand_result );
}
