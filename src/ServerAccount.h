#pragma once

class ServerAccount : public ItemContainer<200>
{
public:
	INLINE std::string& GetName() { return m_strAccountID; }

	ServerAccount(const std::string& strAccountID);

	void Load(std::unique_ptr<OdbcCommand>& dbCommand);
	void AddCharacter(std::string& strCharID);

protected:
	std::string m_strAccountID;
	std::vector<std::string> m_strCharacters;
};
