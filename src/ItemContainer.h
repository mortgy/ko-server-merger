#pragma once

template <int ItemCount>
class ItemContainer
{
public:
	ItemContainer()
	{
		memset(m_strItem, 0, sizeof(m_strItem));
		memset(m_strSerial, 0, sizeof(m_strSerial));
	}

	bool FixDuplicateSerials(SerialSet& serialSet)
	{
		bool duplicateFound = false;
		for (int i = 0; i < ItemCount; i++)
		{
			uint64 nSerialNum = m_strSerial[i];
			if (m_strItem[i].nItemID == 0    /* no item       */
				|| nSerialNum == 0           /* no serial     */
				|| nSerialNum == 999999)     /* rental serial */
				continue;

			auto result = serialSet.insert(nSerialNum);

			// Found collision, change serial so it's unused.
			if (!result.second)
			{
				MYINT64 serial;
				serial.i = nSerialNum;
				++serial.b[7]; /* server number */

				result = serialSet.insert(serial.i);
				while (!result.second)
				{
					printf("Warning: Duplicate found in same database. Generating new serial for " I64FMTD ".\n",
						nSerialNum);

					serial.b[6] = myrand(100, 255); /* year */
					serial.b[5] = myrand(12, 255);  /* month */
					serial.b[4] = myrand(32, 255);  /* day of month */
					serial.b[3] = myrand(24, 255);  /* hour */
					serial.b[2] = myrand(60, 255);  /* minute */

					result = serialSet.insert(serial.i);
				}

				m_strSerial[i] = serial.i;
#ifdef _DEBUG
				printf("[%d] Changing serial from " I64FMTD " to " I64FMTD "\n", 
					i, nSerialNum, serial.i);
#endif
				duplicateFound = true;
			}
		}

		return duplicateFound;
	}

	void SaveSerials(OdbcConnection& dbConnection, const char * updateStmt, std::string& user)
	{
		std::unique_ptr<OdbcCommand> dbCommand(dbConnection.CreateCommand(false));
		if (dbCommand.get() == nullptr)
			return;

		dbCommand->AddInputParameter((const uint8 *) &m_strSerial, sizeof(m_strSerial));
		dbCommand->AddInputParameter(user);
		if (!dbCommand->Execute(updateStmt))
		{
			printf("Error saving serials for user %s:\n", user.c_str());
			printf("Error is: %s\n", dbCommand->GetError());
			printf("Statement: %s\n", updateStmt);
		}
	}

protected:
	RawItemData m_strItem[ItemCount];
	uint64		m_strSerial[ItemCount];
};
