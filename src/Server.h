#pragma once

#include "Character.h"
#include "ServerAccount.h"

class Accounts;
class Server
{
public:
	INLINE OdbcConnection& GetDB() { return m_dbConnection; }

	Server(std::string dsn, std::string uid, std::string pwd);

	bool Connect();

	void LoadAccount(std::string& strAccountID, std::string strCharIDs[3]);

	bool LoadItems();
	bool LoadInventories();
	bool LoadInns();

	void FixDuplicateSerials(SerialSet& serialSet);

protected:
	typedef ci_map<Character>::type CharacterMap;
	typedef ci_map<ServerAccount>::type AccountMap;

	Accounts * m_accounts;
	OdbcConnection m_dbConnection;
	CharacterMap m_characterMap;
	AccountMap m_accountMap;
};
