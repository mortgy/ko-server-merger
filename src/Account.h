#pragma once

class Account
{
public:
	Account(const std::string& strAccountID);

protected:
	std::string m_strAccountID;
};
