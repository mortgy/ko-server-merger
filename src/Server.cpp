#include "stdafx.h"

Server::Server(std::string dsn, std::string uid, std::string pwd)
{
	m_dbConnection.SetConnectionDetails(dsn, uid, pwd);
}

bool Server::Connect()
{
	const char * datasource = m_dbConnection.GetDatasource();

	if (!m_dbConnection.Connect())
	{
		printf("Failed to connect to `%s` datasource.\n", datasource);
		return false;
	}

	printf("Connected to `%s` datasource.\n", datasource);
	return true;
}

void Server::LoadAccount(std::string& strAccountID, std::string strCharIDs[3])
{
	auto& itr = m_accountMap.emplace(strAccountID, strAccountID);
	ServerAccount* account = &(itr.first)->second;

	for (int i = 0; i < 3; i++)
	{
		std::string& strCharID = strCharIDs[i];
		if (strCharID.empty())
			continue;

		m_characterMap.insert(std::make_pair(strCharID, std::move(Character(account, strCharID))));
		account->AddCharacter(strCharID);
	}
}

bool Server::LoadItems()
{
	if (!LoadInventories())
	{
		printf("Failed to load inventories.\n");
		return false;
	}

	if (!LoadInns())
	{
		printf("Failed to load inns.\n");
		return false;
	}

	return true;
}

bool Server::LoadInventories()
{
	OdbcConnection& db = GetDB();
	std::unique_ptr<OdbcCommand> dbCommand(db.CreateCommand(false));
	if (dbCommand.get() == nullptr)
		return false;

	if (!dbCommand->Execute("SELECT strUserID, strItem, strSerial, Level, Exp FROM USERDATA"))
	{
		printf("Error: %s\n", dbCommand->GetError());
		return false;
	}

	size_t count = 0;
	do
	{
		std::string strUserID;
		dbCommand->Fetch(1, strUserID);
		auto itr = m_characterMap.find(strUserID);
		if (itr != m_characterMap.end())
			itr->second.Load(dbCommand);

		++count;
	} while (dbCommand->MoveNext());

	printf("Inventories loaded from `%s`: %u\n", 
		db.GetDatasource(), count);

	return true;
}

bool Server::LoadInns()
{
	OdbcConnection& db = GetDB();
	std::unique_ptr<OdbcCommand> dbCommand(db.CreateCommand(false));
	if (dbCommand.get() == nullptr)
		return false;

	if (!dbCommand->Execute("SELECT strAccountID, WarehouseData, strSerial FROM WAREHOUSE"))
	{
		printf("Error: %s\n", dbCommand->GetError());
		return false;
	}

	size_t count = 0;
	do
	{
		std::string strAccountID;
		dbCommand->Fetch(1, strAccountID);
		auto itr = m_accountMap.find(strAccountID);
		if (itr != m_accountMap.end())
			itr->second.Load(dbCommand);

		++count;
	} while (dbCommand->MoveNext());

	printf("Inns loaded from `%s`: %u\n", 
		db.GetDatasource(), count);

	return true;
}

void Server::FixDuplicateSerials(SerialSet& serialSet)
{
	// Inventories
	foreach (itr, m_characterMap)
	{
		auto& user = itr->second;

		// Found collision, need to save serials.
		if (user.FixDuplicateSerials(serialSet))
			user.SaveSerials(GetDB(), "UPDATE USERDATA SET strSerial = ? WHERE strUserID = ?",
			user.GetName());
	}

	// Inn data
	foreach (itr, m_accountMap)
	{
		auto& user = itr->second;

		// Found collision, need to save serials.
		if (user.FixDuplicateSerials(serialSet))
			user.SaveSerials(GetDB(), "UPDATE WAREHOUSE SET strSerial = ? WHERE strAccountID = ?", 
			user.GetName());
	}
}
