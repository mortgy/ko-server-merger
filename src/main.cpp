#include "stdafx.h"

int main(int argc, char** argv)
{
	Merger app;
	int result = 1;
	srand((uint32) time(NULL));

	uint32 startTime = GetTickCount();
	if (app.Run())
	{
		printf("Took %ums.\n", GetTickCount() - startTime);
		result = 0;
	}

	system("pause");
	return result;
}
