#pragma once

class ServerAccount;
class Character : public ItemContainer<50>
{
public:
	INLINE std::string& GetName() { return m_strCharID; }

	Character(ServerAccount * account, const std::string& strCharID);
	void Load(std::unique_ptr<OdbcCommand>& dbCommand);

protected:
	ServerAccount *	m_account;
	std::string		m_strCharID;
	uint8			m_byLevel;
	uint8			m_nExp;
};
