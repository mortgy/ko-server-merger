#include "stdafx.h"

Character::Character(ServerAccount * account, const std::string& strCharID)
	: ItemContainer()
{
	m_account = account;
	m_strCharID = strCharID;
}

void Character::Load(std::unique_ptr<OdbcCommand>& dbCommand)
{
	dbCommand->FetchBinary(2, reinterpret_cast<uint8 *>(&m_strItem), sizeof(m_strItem));
	dbCommand->FetchBinary(3, reinterpret_cast<uint8 *>(&m_strSerial), sizeof(m_strSerial));
	dbCommand->Fetch(4, m_byLevel);
	dbCommand->Fetch(5, m_nExp);
}
