#include "stdafx.h"

bool Accounts::Load(Server * server)
{
	OdbcConnection& db = server->GetDB();
	std::unique_ptr<OdbcCommand> dbCommand(db.CreateCommand(false));
	if (dbCommand.get() == nullptr)
		return false;

	if (!dbCommand->Execute("SELECT strAccountID, strCharID1, strCharID2, strCharID3 FROM ACCOUNT_CHAR"))
	{
		printf("Error: %s\n", dbCommand->GetError());
		return false;
	}

	size_t accounts = 0;
	do
	{
		std::string strAccountID, strCharIDs[3];

		dbCommand->Fetch(1, strAccountID);
		dbCommand->Fetch(2, strCharIDs[0]);
		dbCommand->Fetch(3, strCharIDs[1]);
		dbCommand->Fetch(4, strCharIDs[2]);

		m_accountMap.emplace(strAccountID, strAccountID);

		server->LoadAccount(strAccountID, strCharIDs);
		++accounts;
	} while (dbCommand->MoveNext());

	printf("Accounts loaded from `%s`: %u\n", 
		db.GetDatasource(), accounts);

	return true;
}

Account * Accounts::LookupAccount(const std::string& strAccountID)
{
	auto itr = m_accountMap.find(strAccountID);
	if (itr == m_accountMap.end())
		return nullptr;

	return &itr->second;
}
