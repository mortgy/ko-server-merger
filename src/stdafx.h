#pragma once

#define NO_THREADING 1

#include "shared/Common.h"
#include "shared/database/OdbcConnection.h"

#include "ItemData.h"
#include "ItemContainer.h"
#include "Server.h"
#include "Accounts.h"
#include "Merger.h"
