#pragma once

struct ci_compare
{
	bool operator() (const std::string & str1, const std::string & str2) const {
		return STRCASECMP(str1.c_str(), str2.c_str()) < 0;
	}
};

template <typename T>
// Map indexed by case-insensitive string.
struct ci_map {
	typedef typename std::map<std::string, T, ci_compare> type;
};

// Case-insensitive string set.
typedef std::set<std::string, ci_compare> ci_set;

std::string & rtrim(std::string &s);
std::string & ltrim(std::string &s);
void strtolower(std::string& str);
void strtoupper(std::string& str);
int myrand(int min, int max);
