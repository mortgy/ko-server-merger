#include "stdafx.h"

ServerAccount::ServerAccount(const std::string& strAccountID)
	: ItemContainer()
{
	m_strAccountID = strAccountID;
}

void ServerAccount::Load(std::unique_ptr<OdbcCommand>& dbCommand)
{
	dbCommand->FetchBinary(2, reinterpret_cast<uint8 *>(&m_strItem), sizeof(m_strItem));
	dbCommand->FetchBinary(3, reinterpret_cast<uint8 *>(&m_strSerial), sizeof(m_strSerial));
}

void ServerAccount::AddCharacter(std::string& strCharID)
{
	m_strCharacters.push_back(strCharID);
}
