#pragma once

#pragma warning(disable:4251)
#pragma warning(disable:4996)
#pragma warning(disable:4018)

#pragma warning(disable:4996)
#define _CRT_SECURE_NO_DEPRECATE 1
#define _CRT_SECURE_COPP_OVERLOAD_STANDARD_NAMES 1
#pragma warning(disable:4251)

#define INLINE __forceinline

#include <stdio.h>
#include <time.h>
#include <memory>
#include <cassert>

#ifdef _WIN32_WINNT
#undef _WIN32_WINNT
#endif

#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x0501
#define NOMINMAX
#include <windows.h>
#undef SendMessage

#define STRCASECMP stricmp

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#include <set>
#include <string>
#include <map>
#include <vector>

// hacky stuff for vc++
#define snprintf _snprintf
#define vsnprintf _vsnprintf

/* Use correct types for x64 platforms, too */
typedef signed __int64 int64;
typedef signed __int32 int32;
typedef signed __int16 int16;
typedef signed __int8 int8;

typedef unsigned __int64 uint64;
typedef unsigned __int32 uint32;
typedef unsigned __int16 uint16;
typedef unsigned __int8 uint8;

#define I64FMT "%016I64X"
#define I64FMTD "%I64u"
#define SI64FMTD "%I64d"
#define snprintf _snprintf
#define atoll __atoi64

#define atol(a) strtoul( a, NULL, 10)

#define STRINGIZE(a) #a

// fix buggy MSVC's for variable scoping to be reliable =S
#define for if(true) for

#define foreach(itr, arr) \
	for (auto itr = arr.begin(); itr != arr.end(); ++itr)

#define reverse_foreach(itr, arr) \
	for (auto itr = arr.rbegin(); itr != arr.rend(); ++itr)

#define foreach_allow_erase(itr, arr) \
	for (auto itr = arr.begin(), _ ## itr = itr; \
		itr != arr.end() && ++(_ ## itr = itr) != itr; itr = _ ## itr)

#define foreach_array(itr, arr) foreach_array_n(itr, arr, sizeof(arr) / sizeof(arr[0]))
#define foreach_array_n(itr, arr, len) for (auto itr = 0; itr < len; itr++)

#include "globals.h"
