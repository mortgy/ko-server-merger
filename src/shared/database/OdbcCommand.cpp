#include "stdafx.h"

OdbcCommand::OdbcCommand(OdbcConnection * conn, bool noCount)
	: m_odbcConnection(conn), m_hStmt(nullptr), m_noCount(noCount)
{
	m_connHandle = conn->GetConnectionHandle();
	m_odbcConnection->AddCommand(this);
}

OdbcParameter::OdbcParameter(SQLSMALLINT parameterType, SQLSMALLINT dataType, SQLPOINTER parameterAddress, SQLLEN maxLength /*= 1*/)
	: m_parameterType(parameterType), m_cDataType(dataType), m_parameterAddress(parameterAddress), 
	m_dataTypeLength(0), m_pCBValue(SQL_NTS)
{
	switch (m_cDataType)
	{
	case SQL_CHAR:
	case SQL_VARCHAR:
	case SQL_BINARY:
		m_dataTypeLength = maxLength;
		m_dataType = m_cDataType;
		if (m_cDataType == SQL_BINARY || m_cDataType == SQL_CHAR)
			m_pCBValue = m_dataTypeLength;
		break;

	case SQL_C_STINYINT:
	case SQL_C_UTINYINT:
		m_dataType = SQL_TINYINT;
		break;

	case SQL_C_SSHORT:
	case SQL_C_USHORT:
		m_dataType = SQL_SMALLINT;
		break;

	case SQL_C_SLONG:
	case SQL_C_ULONG:
		m_dataType = SQL_INTEGER;
		break;

	case SQL_C_FLOAT:
		m_dataType = SQL_FLOAT;
		break;

	case SQL_C_DOUBLE:
		m_dataType = SQL_DOUBLE;
		break;

	default: // unknown, default to integer
		m_dataType = SQL_INTEGER;
		m_cDataType = m_dataType + SQL_SIGNED_OFFSET;
	}
}

bool OdbcCommand::BindParameters()
{
	foreach (itr, m_params)
	{
		auto param = itr->second;

		if (!SQL_SUCCEEDED(SQLBindParameter(m_hStmt, itr->first + 1, param->GetParameterType(), 
			param->GetCDataType(), param->GetDataType(), param->GetDataTypeSize(), 0, 
			param->GetAddress(), param->GetDataTypeSize(), param->GetCBValue())))
		{
			if (m_odbcConnection != nullptr)
				m_szError = m_odbcConnection->ReportSQLError(SQL_HANDLE_STMT, m_hStmt, "SQLBindParameter", "Failed to bind parameter.");
			else
				m_szError = OdbcConnection::GetSQLError(SQL_HANDLE_STMT, m_hStmt);

			Close();
			m_connHandle = nullptr; /* force reconnection next time */
			return false;
		}
	}
	return true;
}

bool OdbcCommand::Open(bool bRetry /*= false*/)
{
	if (IsOpen())
		return true;

	if (m_connHandle == nullptr
		&& m_odbcConnection != nullptr
		&& !AttemptReconnection(bRetry))
		return false;

	if (!SQL_SUCCEEDED(SQLAllocHandle(SQL_HANDLE_STMT, m_connHandle, &m_hStmt)))
		return AttemptReconnection(bRetry);

	if (m_noCount)
		SQLExecDirectA(m_hStmt, (SQLCHAR *)"SET NOCOUNT ON", SQL_NTS);
	return true;
}

bool OdbcCommand::AttemptReconnection(bool bRetry)
{
	if (m_odbcConnection != nullptr)
		m_szError = m_odbcConnection->ReportSQLError(SQL_HANDLE_DBC, m_connHandle, "SQLAllocHandle", "Failed to allocate statement handle.");
	else
		m_szError = OdbcConnection::GetSQLError(SQL_HANDLE_DBC, m_connHandle);

	// Attempt full SQL reconnection once.
	if (m_odbcConnection == nullptr || bRetry)
		return false;
		
	// Perform soft disconnect, preserving existing commands
	m_odbcConnection->Close();

	// Reconnect
	m_odbcConnection->Connect();

	// Now try running the statement once more time.
	return Open(true); 
}

bool OdbcCommand::Prepare(const char * szSQL, ...)
{
	if (!Open())
		return false;

	char buffer[4096];
	va_list ap;
	va_start(ap, szSQL);
	vsnprintf(buffer, sizeof(buffer), szSQL, ap);
	va_end(ap);

#ifdef USE_SQL_TRACE
	TRACE("%s\n", buffer);
#endif

	if (!BindParameters())
		return false;

	SQLRETURN result = SQLPrepare(m_hStmt, (SQLCHAR *)buffer, SQL_NTS);
	if (!(result == SQL_SUCCESS || result == SQL_SUCCESS_WITH_INFO || result == SQL_NO_DATA))
	{
		if (m_odbcConnection != nullptr)
			m_szError = m_odbcConnection->ReportSQLError(SQL_HANDLE_STMT, m_hStmt, buffer, "Failed to execute statement.");
		else
			m_szError = OdbcConnection::GetSQLError(SQL_HANDLE_STMT, m_hStmt);

		Close();
		m_connHandle = nullptr; /* force reconnection next time */
		return false;
	}

	return true;
}

bool OdbcCommand::ExecutePrepared()
{
	if (!IsOpen())
		return false;

	SQLRETURN result = SQLExecute(m_hStmt);
	if (!(result == SQL_SUCCESS || result == SQL_SUCCESS_WITH_INFO || result == SQL_NO_DATA))
	{
		if (m_odbcConnection != nullptr)
			m_szError = m_odbcConnection->ReportSQLError(SQL_HANDLE_STMT, m_hStmt, "<n/a>", "Failed to execute prepared statement.");
		else
			m_szError = OdbcConnection::GetSQLError(SQL_HANDLE_STMT, m_hStmt);

		Close();
		m_connHandle = nullptr; /* force reconnection next time */
		return false;
	}

	return true;
}

bool OdbcCommand::Execute(const char * szSQL, ...)
{
	if (!Open())
		return false;

	char buffer[4096];
	va_list ap;
	va_start(ap, szSQL);
	vsnprintf(buffer, sizeof(buffer), szSQL, ap);
	va_end(ap);

#ifdef USE_SQL_TRACE
	TRACE("%s\n", buffer);
#endif

	if (!BindParameters())
		return false;

	SQLRETURN result = SQLExecDirectA(m_hStmt, (SQLCHAR *)buffer, SQL_NTS);
	if (!(result == SQL_SUCCESS || result == SQL_SUCCESS_WITH_INFO || result == SQL_NO_DATA))
	{
		if (m_odbcConnection != nullptr)
			m_szError = m_odbcConnection->ReportSQLError(SQL_HANDLE_STMT, m_hStmt, buffer, "Failed to execute statement.");
		else
			m_szError = OdbcConnection::GetSQLError(SQL_HANDLE_STMT, m_hStmt);

		Close();
		m_connHandle = nullptr; /* force reconnection next time */
		return false;
	}

	if (!MoveNext())
		MoveNextSet();

	return true;
}

bool OdbcCommand::MoveNext()
{
	if (!IsOpen())
		return false;

	return SQL_SUCCEEDED(m_resultCode = SQLFetch(m_hStmt));
}

bool OdbcCommand::MoveNextSet()
{
	if (!IsOpen())
		return false;

	return SQL_SUCCEEDED(m_resultCode = SQLMoreResults(m_hStmt));
}

void OdbcCommand::AddParameter(SQLSMALLINT paramType, SQLSMALLINT sqlType, SQLPOINTER value, SQLLEN maxLength) {
	m_params.insert(std::make_pair((SQLUSMALLINT) m_params.size(), new OdbcParameter(paramType, sqlType, value, maxLength)));
}

#define IMPL_ODBC_TYPE(type, sqlType) \
	void OdbcCommand::AddInputParameter (type * value) { AddParameter(SQL_PARAM_INPUT, sqlType,  (SQLPOINTER) value, 0); } \
	void OdbcCommand::AddOutputParameter(type * value) { AddParameter(SQL_PARAM_OUTPUT, sqlType, (SQLPOINTER) value, 0); } \
	bool OdbcCommand::Fetch(int pos, type& value) \
	{ \
		SQLLEN cb = SQL_NTS; \
		return SQL_SUCCEEDED(SQLGetData(m_hStmt, pos, sqlType, &value, 0, &cb)); \
	}

IMPL_ODBC_TYPE(  int8, SQL_C_STINYINT);
IMPL_ODBC_TYPE( uint8, SQL_C_UTINYINT);
IMPL_ODBC_TYPE( int16, SQL_C_SSHORT);
IMPL_ODBC_TYPE(uint16, SQL_C_USHORT);
IMPL_ODBC_TYPE( int32, SQL_C_SLONG);
IMPL_ODBC_TYPE(uint32, SQL_C_ULONG);
IMPL_ODBC_TYPE( float, SQL_C_FLOAT);
IMPL_ODBC_TYPE(double, SQL_C_DOUBLE);
IMPL_ODBC_TYPE( int64, SQL_C_SBIGINT);
IMPL_ODBC_TYPE(uint64, SQL_C_UBIGINT);

void OdbcCommand::AddInputParameter(const char * value, size_t len) {
	AddParameter(SQL_PARAM_INPUT, SQL_C_CHAR,   (SQLPOINTER) value, (SQLINTEGER) len);
}

bool OdbcCommand::FetchString(int pos, char * output, SQLLEN maxLength)
{
	SQLLEN cb = maxLength;
	return FetchString(pos, output, maxLength, &cb);
}

bool OdbcCommand::FetchString(int pos, char * output, SQLLEN maxLength, SQLLEN * bufferSize) {
	return SQL_SUCCEEDED(SQLGetData(m_hStmt, pos, SQL_CHAR, output, maxLength, bufferSize));
}

void OdbcCommand::AddInputParameter(const uint8 * value, size_t len) {
	AddParameter(SQL_PARAM_INPUT, SQL_C_BINARY, (SQLPOINTER) value, (SQLINTEGER) len);
}

bool OdbcCommand::FetchBinary(int pos, uint8 * output, SQLLEN maxLength)
{
	SQLLEN cb = maxLength;
	return FetchBinary(pos, output, maxLength, &cb);
}

bool OdbcCommand::FetchBinary(int pos, uint8 * output, SQLLEN maxLength, SQLLEN * bufferSize) {
	return SQL_SUCCEEDED(SQLGetData(m_hStmt, pos, SQL_BINARY, output, maxLength, bufferSize));
}

bool OdbcCommand::Fetch(int pos, std::string& value)
{
	SQLLEN bufferSize = 0;
	char buffer[256] = "";

	// Attempt to fetch "small" string of 256 bytes at most (should fit everything we'll need)
	if (!FetchString(pos, buffer, sizeof(buffer), &bufferSize))
	{
		// Error fetching string, nothing we can do.
		if (bufferSize <= 0)
			return false;

		// Allocate a buffer large enough for the string's actual length
		std::unique_ptr<char> varBuffer(new char[bufferSize + 1]);

		// If the string still couldn't be fetched, nothing we can do.
		if (!FetchString(pos, varBuffer.get(), bufferSize + 1, &bufferSize))
			return false;

		value = varBuffer.get();
	}
	// String could be fetched, copy it over to the output var.
	else
	{
		value = buffer;
	}

	// This line's necessary for SQL_CHAR type columns to trim the padding.
	// NOTE: If we need the padding, we should be using the OTHER FetchString() method for it (to fetch it straight into a char array)
	rtrim(value);
	return true;
}

void OdbcCommand::ClearParameters()
{
	if (m_params.empty())
		return;

	foreach (itr, m_params)
		delete itr->second;

	m_params.clear();
}

void OdbcCommand::Close()
{
	if (!IsOpen())
		return;

	SQLCloseCursor(m_hStmt); // free results, if any
	SQLFreeHandle(SQL_HANDLE_STMT, m_hStmt);
	m_hStmt = nullptr;
}

void OdbcCommand::Detach()
{
	m_odbcConnection = nullptr;
}

OdbcCommand::~OdbcCommand()
{
	Close();
	ClearParameters();

	if (m_odbcConnection != nullptr)
		m_odbcConnection->RemoveCommand(this);
}
