#include "stdafx.h"
#include "OdbcConnection.h"

OdbcConnection::OdbcConnection()
	: m_connHandle(nullptr), m_envHandle(nullptr) 
{
#if !defined(NO_THREADING)
	m_bMarsEnabled = false;
#endif
}

bool OdbcConnection::isConnected() 
{
#if !defined(NO_THREADING)
	Guard<Mutex> lock(m_lock);
#endif
	return (m_connHandle != nullptr);
}

bool OdbcConnection::isError() 
{
#if !defined(NO_THREADING)
	Guard<Mutex> lock(m_lock);
#endif
	return (!m_odbcErrors.empty());
}

#if !defined(NO_THREADING)
void OdbcConnection::SetConnectionDetails(std::string & szDSN, std::string & szUser, std::string & szPass, bool bMarsEnabled /*= true*/)
#else
void OdbcConnection::SetConnectionDetails(std::string & szDSN, std::string & szUser, std::string & szPass)
#endif
{
	m_szDSN = szDSN;
	m_szUser = szUser;
	m_szPass = szPass;

#if !defined(NO_THREADING)
#	if defined(WIN32) && !defined(USE_MYSQL)
	m_bMarsEnabled = bMarsEnabled;
#	else // MARS is not supported by FreeTDS
	m_bMarsEnabled = false;
#	endif
#endif
}

#if !defined(NO_THREADING)
bool OdbcConnection::Connect(std::string & szDSN, std::string & szUser, std::string & szPass, bool bMarsEnabled /*= true*/)
{
	SetConnectionDetails(szDSN, szUser, szPass, bMarsEnabled);
	return Connect();
}
#else
bool OdbcConnection::Connect(std::string & szDSN, std::string & szUser, std::string & szPass)
{
	SetConnectionDetails(szDSN, szUser, szPass);
	return Connect();
}
#endif

bool OdbcConnection::Connect()
{
	if (m_szDSN.empty())
		return false;

#if !defined(NO_THREADING)
	Guard<Mutex> lock(m_lock);
#endif

	std::string szConn = "DSN=" + m_szDSN + ";";

	// Reconnect if we need to.
	if (isConnected())
		Disconnect(false);

	// Allocate enviroment handle
	if (!SQL_SUCCEEDED(SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &m_envHandle)))
	{
		ReportSQLError(SQL_HANDLE_ENV, m_envHandle, "SQLAllocHandle", "Unable to allocate environment handle.");
		goto error_handler;
	}

	// Request ODBC3 support
	if (!SQL_SUCCEEDED(SQLSetEnvAttr(m_envHandle, SQL_ATTR_ODBC_VERSION, (void *)SQL_OV_ODBC3, 0)))
	{
		ReportSQLError(SQL_HANDLE_ENV, m_envHandle, "SQLSetEnvAttr", "Unable to set environment attribute (SQL_ATTR_ODBC_VERSION).");
		goto error_handler;
	}

	// Allocate the connection handle
	if (!SQL_SUCCEEDED(SQLAllocHandle(SQL_HANDLE_DBC, m_envHandle, &m_connHandle)))
	{
		ReportSQLError(SQL_HANDLE_ENV, m_envHandle, "SQLAllocHandle", "Unable to allocate connection handle.");
		goto error_handler;
	}

	if (!m_szUser.empty())
	{
		szConn += "UID=" + m_szUser + ";";
		if (!m_szPass.empty())
			szConn += "PWD=" + m_szPass + ";";
	}

#if !defined(NO_THREADING)
	// Enable multiple active result sets
	if (m_bMarsEnabled)
	{
		if (!SQL_SUCCEEDED(SQLSetConnectAttr(m_connHandle, SQL_COPT_SS_MARS_ENABLED, SQL_MARS_ENABLED_YES, SQL_IS_UINTEGER)))
		{
			printf("** WARNING **\n\n");
			printf("Attempted to used MARS (Multiple Active Result Sets), but this\n");
			printf("feature is not supported by your ODBC driver or SQL Server version.\n\n");
			printf("To benefit from MARS, you need to be using at least SQL Server 2005, and at\n");
			printf("least the 'SQL Native Client' ODBC driver (as opposed to the vastly outdated\n'SQL Server' driver).\n\n");
			printf("Continuing to connect without MARS.\n\n");
			m_bMarsEnabled = false;
		}

		// NOTE: We can enable MARS via specifying the following, but we are unable to detect if it's supported this way.
		// szConn += "MARS_Connection=yes;";
	}
#endif

	if (!SQL_SUCCEEDED(SQLDriverConnectA(m_connHandle, SQL_NULL_HANDLE, (SQLCHAR *)szConn.c_str(), SQL_NTS, 0, 0, 0, 0)))
	{
		ReportSQLError(SQL_HANDLE_DBC, m_connHandle, "SQLDriverConnect", "Unable to establish connection.");
		goto error_handler;
	}

	foreach (itr, m_commandSet)
		(*itr)->SetConnectionHandle(m_connHandle);

	return true;

error_handler:
	ResetHandles();
	return false;
}

OdbcCommand *OdbcConnection::CreateCommand(bool noCount /*= true*/)
{
	if (!isConnected()
		&& !Connect())
		return nullptr;

	return new OdbcCommand(this, noCount);
}

void OdbcConnection::AddCommand(OdbcCommand *dbCommand)
{
#if !defined(NO_THREADING)
	Guard<Mutex> lock(m_lock);
#endif
	m_commandSet.insert(dbCommand);
}

void OdbcConnection::RemoveCommand(OdbcCommand *dbCommand)
{
#if !defined(NO_THREADING)
	Guard<Mutex> lock(m_lock);
#endif
	m_commandSet.erase(dbCommand);
}

// Used to internally reset handles. Should ONLY be used in special cases, otherwise we'll break the state of the connection.
void OdbcConnection::ResetHandles()
{
	// Free the connection handle if it's allocated
	if (m_connHandle != nullptr)
	{
		SQLFreeHandle(SQL_HANDLE_DBC, m_connHandle);
		m_connHandle = nullptr;
	}

	// Free the environment handle if it's allocated
	if (m_envHandle != nullptr)
	{
		SQLFreeHandle(SQL_HANDLE_ENV, m_envHandle);
		m_envHandle = nullptr;
	}
}

std::string OdbcConnection::ReportSQLError(
	SQLSMALLINT handleType, SQLHANDLE handle,
	const char *szSource, const char *szError, ...)
{
#if !defined(NO_THREADING)
	Guard<Mutex> lock(m_lock);
#endif
	char szErrorBuffer[256];
	OdbcError *error = new OdbcError();

	va_list args;
	va_start(args, szError);
	vsnprintf(szErrorBuffer, sizeof(szErrorBuffer), szError, args);
	va_end(args);

	error->Source = szSource;
	error->ErrorMessage = szErrorBuffer;

	m_odbcErrors.push_back(error);

	if (handle != nullptr)
	{
		error->ExtendedErrorMessage = GetSQLError(handleType, handle);
		if (!error->ExtendedErrorMessage.empty())
			return error->ExtendedErrorMessage;
	}
	
	return szErrorBuffer;
}

std::string OdbcConnection::GetSQLError(SQLSMALLINT handleType, SQLHANDLE handle)
{
	std::string result;
	SQLCHAR SqlState[256], SqlMessage[256];
	SQLINTEGER NativeError;
	SQLSMALLINT TextLength;

	if (SQL_SUCCEEDED(SQLGetDiagRecA(handleType, handle, 1, (SQLCHAR *)&SqlState, &NativeError, 
			(SQLCHAR *)&SqlMessage, sizeof(SqlMessage), &TextLength)))
		result = (char *)SqlMessage;

	return result;
}

OdbcError *OdbcConnection::GetError()
{
#if !defined(NO_THREADING)
	Guard<Mutex> lock(m_lock);
#endif
	if (m_odbcErrors.empty())
		return nullptr;

	OdbcError *pError = m_odbcErrors.back();
	m_odbcErrors.pop_back();
	return pError;
}

void OdbcConnection::ResetErrors()
{
	if (!isError())
		return;

#if !defined(NO_THREADING)
	Guard<Mutex> lock(m_lock);
#endif
	OdbcError *pError;
	while ((pError = GetError()) != nullptr)
		delete pError;
}

void OdbcConnection::Disconnect(bool killOpenStatements /*= false*/)
{
	// Make sure our handles are open. If not, there's nothing to do.
	if (!isConnected())
		return;

#if !defined(NO_THREADING)
	Guard<Mutex> lock(m_lock);
#endif

	// Kill off open statements
	if (killOpenStatements 
		&& !m_commandSet.empty())
	{
		foreach (itr, m_commandSet)
		{
			// Detach from the connection first so we don't try to remove it from the set (while we're using it!)
			(*itr)->Detach();

			// Now free it.
			delete (*itr);
		}

		m_commandSet.clear();
	}

	// Close the connection to the server & reset our handles
	Close();
}

void OdbcConnection::Close()
{
	// Disconnect from server.
	SQLDisconnect(m_connHandle);

	// Reset handles
	ResetHandles();
}

OdbcConnection::~OdbcConnection()
{
	Disconnect(true);
	ResetErrors();
}
