#pragma once

#pragma comment(lib, "odbc32.lib")

#include <sqlext.h>
#include <set>

// MARS settings from sqlncli.h
#ifndef SQL_COPT_SS_MARS_ENABLED
#	define SQL_COPT_SS_MARS_ENABLED 1224
#endif

#ifndef SQL_MARS_ENABLED_YES
#	define SQL_MARS_ENABLED_YES (SQLPOINTER)1
#endif

struct OdbcError
{
	std::string	Source;
	std::string	ErrorMessage;
	std::string	ExtendedErrorMessage;
};

#include "OdbcCommand.h"

class OdbcConnection
{
	friend class OdbcCommand;

public:
	OdbcConnection();

	bool isConnected();
	bool isError();

	INLINE HDBC GetConnectionHandle() { return m_connHandle; }
	INLINE const char * GetDatasource() { return m_szDSN.c_str(); }
#if !defined(NO_THREADING)
	INLINE bool isMarsEnabled() { return m_bMarsEnabled; }
#endif

#if !defined(NO_THREADING)
	void SetConnectionDetails(std::string & szDSN, std::string & szUser, std::string & szPass, bool bMarsEnabled = true);
	bool Connect(std::string & szDSN, std::string & szUser, std::string & szPass, bool bMarsEnabled = true);
#else
	void SetConnectionDetails(std::string & szDSN, std::string & szUser, std::string & szPass);
	bool Connect(std::string & szDSN, std::string & szUser, std::string & szPass);
#endif

	bool Connect();

	OdbcCommand *CreateCommand(bool noCount = true);
	static std::string GetSQLError(SQLSMALLINT handleType, SQLHANDLE handle);

	OdbcError *GetError();
	void ResetErrors();

	void Disconnect(bool killOpenStatements = false);
	~OdbcConnection();

private:
	void AddCommand(OdbcCommand *dbCommand);
	void RemoveCommand(OdbcCommand *dbCommand);
	std::string ReportSQLError(SQLSMALLINT handleType, SQLHANDLE handle, const TCHAR *szSource, const char *szError, ...);

	void Close();
	void ResetHandles();

private:
	std::string m_szDSN, m_szUser, m_szPass;

	HENV m_envHandle;
	HDBC m_connHandle;

	std::vector<OdbcError   *> m_odbcErrors;
	std::set   <OdbcCommand *> m_commandSet;

#if !defined(NO_THREADING)
	Mutex m_lock;
	bool m_bMarsEnabled;
#endif
};
