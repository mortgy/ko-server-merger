#pragma once

// The MySQL ODBC driver doesn't appear to support calls via curly braces.
// Ordinarily, ODBC is meant to evaluate these as function calls and translate it to native SQL code.
// Instead, with MySQL, it just seems to do nothing/disregard the result set...
// Intended behaviour can be reproduced without the curly braces, however this isn't supported by ODBC/MSSQL
// so there doesn't appear much we can do here, other than replace the braces at compile-time.
// NOTE: If we knew what type of connection at runtime, we could also generate the string then, but you'll only
// ever be using one or the other anyway, so I don't see much point in bothering.
#if defined(USE_MYSQL)
#	define CALL_PROC(call)		"CALL " call
#	define ODBC_COL(name)		"`" name "`"
#else
#	define CALL_PROC(call)		"{CALL " call "}"
#	define ODBC_COL(name)		"[" name "]"
#endif

class OdbcParameter
{
public:
	OdbcParameter(SQLSMALLINT parameterType, SQLSMALLINT dataType, SQLPOINTER parameterAddress, SQLLEN maxLength = 1);

	INLINE SQLSMALLINT GetParameterType() { return m_parameterType; };
	INLINE SQLSMALLINT GetDataType() { return m_dataType; };
	INLINE SQLSMALLINT GetCDataType() { return m_cDataType; };
	INLINE SQLPOINTER GetAddress() { return m_parameterAddress; };
	INLINE SQLLEN GetDataTypeSize() { return m_dataTypeLength; };
	INLINE SQLLEN *GetCBValue() { return &m_pCBValue; };

private:
	SQLSMALLINT m_parameterType, m_dataType, m_cDataType;
	SQLPOINTER m_parameterAddress;
	SQLLEN m_dataTypeLength, m_pCBValue;
};

class OdbcConnection;
class OdbcCommand
{
public:
	typedef std::map<SQLUSMALLINT, OdbcParameter *> OdbcParameterCollection;

	OdbcCommand(OdbcConnection * conn, bool noCount);

	INLINE bool IsOpen() { return m_hStmt != nullptr; };
	INLINE const char * GetError() { return m_szError.c_str();  };
	INLINE bool HasData() { return m_resultCode != SQL_NO_DATA && SQL_SUCCEEDED(m_resultCode); };
	INLINE void SetConnectionHandle(HDBC handle) { m_connHandle = handle; };
	INLINE OdbcConnection* GetConnection() { return m_odbcConnection; }

private:
	bool BindParameters();
	bool AttemptReconnection(bool bRetry);

public:
	bool Prepare(const char* szSQL, ...);
	bool ExecutePrepared();

	bool Execute(const char* szSQL, ...);
	bool MoveNext();
	bool MoveNextSet();

	void AddParameter(SQLSMALLINT paramType, SQLSMALLINT sqlType, SQLPOINTER value, SQLLEN maxLength);

	#define DECL_ODBC_TYPE(type) \
	void AddInputParameter(type  * value); \
	void AddOutputParameter(type * value); \
	bool Fetch(int pos, type& value);

	DECL_ODBC_TYPE(  int8);
	DECL_ODBC_TYPE( uint8);
	DECL_ODBC_TYPE( int16);
	DECL_ODBC_TYPE(uint16);
	DECL_ODBC_TYPE( int32);
	DECL_ODBC_TYPE(uint32);
	DECL_ODBC_TYPE( float);
	DECL_ODBC_TYPE(double);
	DECL_ODBC_TYPE( int64);
	DECL_ODBC_TYPE(uint64);
	#undef DECL_ODBC_TYPE

	// Backwards compatibility (TODO: replace & remove)
	#define DECL_EXPLICIT_ODBC_TYPE(type, typeName) \
		INLINE bool Fetch ## typeName(int pos, type& value) { return Fetch(pos, value); }

	DECL_EXPLICIT_ODBC_TYPE(  int8, SByte);
	DECL_EXPLICIT_ODBC_TYPE( uint8, Byte);
	DECL_EXPLICIT_ODBC_TYPE( int16, Int16);
	DECL_EXPLICIT_ODBC_TYPE(uint16, UInt16);
	DECL_EXPLICIT_ODBC_TYPE( int32, Int32);
	DECL_EXPLICIT_ODBC_TYPE(uint32, UInt32);
	DECL_EXPLICIT_ODBC_TYPE( int64, Int64);
	DECL_EXPLICIT_ODBC_TYPE(uint64, UInt64);
	DECL_EXPLICIT_ODBC_TYPE( float, Single);
	DECL_EXPLICIT_ODBC_TYPE(double, Double);
	DECL_EXPLICIT_ODBC_TYPE(std::string, String);

	// Other common types
	bool Fetch(int pos, std::string& value);
	void AddInputParameter(const char  * value, size_t len);
	void AddInputParameter(const uint8 * value, size_t len);

	INLINE void AddInputParameter (std::string& value) {
		AddInputParameter(value.c_str(), value.length());  
	}

	INLINE void AddInputParameter(const char * value) {
		AddInputParameter(value, strlen(value));
	}

	bool FetchString(int pos, char  * output, SQLLEN maxLength);
	bool FetchString(int pos, char  * output, SQLLEN maxLength, SQLLEN *bufferSize);

	bool FetchBinary(int pos, uint8 * output, SQLLEN maxLength);
	bool FetchBinary(int pos, uint8 * output, SQLLEN maxLength, SQLLEN *bufferSize);

	void Detach();
	~OdbcCommand();

private:
	bool Open(bool bRetry = false);
	void ClearParameters();
public:	void Close();
private:
	HDBC m_connHandle;
	OdbcConnection *m_odbcConnection;
	HSTMT m_hStmt;
	bool		m_noCount;

	OdbcParameterCollection m_params;

	std::string m_szError;
	SQLRETURN	m_resultCode;
};
